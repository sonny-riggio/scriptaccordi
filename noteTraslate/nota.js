
function LeNote (nota) {

	noteItaDiesis = ["LA", "LA#", "SI", "DO", "DO#", "RE", "RE#", "MI", "FA", "FA#", "SOL", "SOL#"];
	noteItaBemolle = ["LA", "SIb", "SI", "DO","REb", "RE", "MIb", "MI", "FA", "SOLb", "SOL", "LAb"];

	noteEngDiesis = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"];
	noteEngBemolle = ["A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab"];

	this.nota;
	this.restOfNota;
	this.language = "ITA";
	this.accident;
	this.indexAccident;
	this.index;

	//INDIVIDUO TUTTE LE PARTI DELLA NOTA = ACCIDENT ed il suo INDICE, LA NOTA ED IL SUO RESTO
	this.individuaNota = function(testo){

		//IMPOSTO LA LINGUA
		if(testo.length == 1){
			this.language = "ENG";
		}

		if(testo[0] == "A" || testo[0] == "B" || testo[0] == "C" || testo[0] == "E" || testo[0] == "G"){
			this.language = "ENG";
		}

		if(testo.length == 2 || testo.length == 3 || testo.length == 4 || testo.length == 5 || testo.length == 6){
			if(testo[0] == "D" || testo[0] == "F"){
				this.language = "ENG";
				if (testo[1] == "O" || testo[1] == "A") {
					this.language = "ITA";
				}
			}
		}

		//PRENDO L'INDICE DELL DIESIS O BEMOLLE SALVANDOMI ANCHE IL RESTO DELLA NOTA
		if(testo.indexOf("#")> -1){
			this.indexAccident = testo.indexOf("#"); 
			this.accident = "#";
			this.restOfNota = testo.substr(this.indexAccident+1);
			this.nota = testo.substr(0, this.indexAccident);
		}
		else if (testo.indexOf("b")> -1){
			this.indexAccident = testo.indexOf("b");
			this.accident = "b";
			this.restOfNota = testo.substr(this.indexAccident+1);
			this.nota = testo.substr(0, this.indexAccident);
		}
		else if(testo.indexOf("SOL")> -1){
			this.restOfNota = testo.substr(3);
			this.nota = testo.substr(0, 3);
		}
		else if(testo.length >= 2 && this.language == "ITA"){
			this.restOfNota = testo.substr(2);
			this.nota = testo.substr(0, 2);
		}
		else if(testo.length >= 1 && this.language == "ENG"){
			this.restOfNota = testo.substr(1);
			this.nota = testo.substr(0, 1);
		}
	}


//SALVO L'INDICE DELLA NOTA SULL MIO ARRAY DI NOTE A SECONDA SE SI PARLA DI DIESIS O BEMOLLE
	this.takeIndex = function() {

		if(this.accident == undefined){
			var notaComposta = this.nota;
		}else{
			notaComposta = this.nota+this.accident
		}

		for (i = 0; i < noteEngDiesis.length; i++) {
			if (noteEngDiesis[i] == notaComposta){
				this.index = i;
				return this.index;
			}
		}

		for (i = 0; i < noteItaDiesis.length; i++) {
			if (noteItaDiesis[i] == notaComposta){
				this.index = i;
				return this.index;
			}
		}

		for (i = 0; i < noteEngBemolle.length; i++) {
			if (noteEngBemolle[i] == notaComposta){
				this.index = i;
				return this.index;
			}
		}

		for (i = 0; i < noteItaBemolle.length; i++) {
			if (noteItaBemolle[i] == notaComposta){

				this.index = i;
				return this.index;
			}
		}
	}


	//TRADUCO E MI SPOSTO DI SEMITONI IN CONTEMPORANEA PASSANDOGLI COME PARAMETRI LA LINGUA SCELTA DELLA TRADUZIONE ED IL NUMERO DI SEMITONI DA SPOSTARE
	this.traslateAndTraspose = function (lang, semitono) {

		var newIndex = this.getindexForTraspose(semitono);

		if(lang == "ENG"){
			if(this.accident == "#"){
				this.nota = noteEngDiesis[newIndex];
				this.language = "ENG";
			}

			else if(this.accident == "b"){
				this.nota = noteEngBemolle[newIndex];
				this.language = "ENG";
			}else{
			this.nota = noteEngDiesis[newIndex];
			this.language = "ENG";
			}
		}
		else if(lang == "ITA"){
			if(this.accident == "#"){
				this.nota = noteItaDiesis[newIndex];
				this.language = "ITA";
			}

			else if(this.accident == "b"){
				this.nota = noteItaBemolle[newIndex];
				this.language = "ITA";
			}else{
			this.nota = noteItaDiesis[newIndex];
			this.language = "ITA";
			}
		}
		var tuttaLaNota = this.nota+this.restOfNota;
		return this.nota+this.restOfNota;
	}

	//FUNZIONE CHE OTTIENE L'INDICE UTILIZZANDOLA PER this.traslateAndTraspose
	this.getindexForTraspose = function(indexin){

		var newIndex = (parseInt(indexin)+this.index);
		while(newIndex <= 0){
			newIndex = newIndex+noteItaDiesis.length;
		}
		return newIndex = newIndex%noteItaDiesis.length;			
	}

	this.individuaNota(nota);
	this.takeIndex();
}