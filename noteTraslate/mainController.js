var lines;
window.onload = function() {
	var fileInput = document.getElementById('fileInput');
	var fileDisplayArea = document.getElementById('fileDisplayArea');

	fileInput.addEventListener('change', function(e) {
		var file = fileInput.files[0];
		var textType = /text.*/;

		if (file.type.match(textType)) {
			var reader = new FileReader();

			reader.onload = function(e) {
				lines = reader.result.split("\n");
				fileDisplayArea.innerHTML = "";
				for(var i = 0; i < lines.length; i++){
					var checkbox = "<input id='checkbox_"+i+"' type='checkbox' />"
					var stringa = "<div class='testo' id='testo_"+i+"'>"+lines[i]+"</div>"
					fileDisplayArea.innerHTML += checkbox + stringa;	
				}
			}
			reader.readAsText(file);	
		} else {
			fileDisplayArea.innerText = "File not supported!"
		}
	});
}



var arrayLineString = [];
function traslateAndTraspose(){
	var inputTendina = document.getElementById('inputTendina').value;
	var inputSemitono = document.getElementById('semitono').value;
	for(var i = 0; i < arrayLineString.length; i++){
		var temp = arrayLineString[i].myString;
		var tradotto = temp.traslateAndTraspose(inputTendina, inputSemitono);
		var rigaDaSostituire = document.getElementById("testo_"+arrayLineString[i].index);
		rigaDaSostituire.innerHTML = tradotto+"\n";
	}
}


function elabora(){
	console.log('ci sono dentro');

	//var lines = document.getElementById("fileDisplayArea").textContent.split("\n");

	for(var i = 0; i < lines.length; i++){
		checkbox = document.getElementById("checkbox_"+i);
		if(checkbox.checked){
			document.getElementById("testo_"+i).className += ' red';
			var stringChord = lines[i].replace("\r", "");  //document.getElementById("testo_"+i).textContent;
			var rowNotes = new stringRowNote(stringChord);

			var lineString = {
				myString: rowNotes,
				index: i
			}
			arrayLineString.push(lineString);
			console.log("SONO ARRAYLINESTRING    "+arrayLineString);
		}
		document.getElementById("checkbox_"+i).className += ' hide-checkbox';
	}
}